package org.nrg.structuralqc.components.pcp;

import java.util.Date;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelinePrereqCheckerI;
import org.nrg.structuralqc.components.ProjectSettings;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelinePrereqChecker
public class StructuralQcPrereqChecker implements PipelinePrereqCheckerI {
	
	private ProjectSettings _projectSettings = XDAT.getContextService().getBean(ProjectSettings.class);;
	
	@Override
	public void checkPrereqs(PcpStatusEntity statusEntity, UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(statusEntity.getProject());
		final Boolean prereqs = statusEntity.getPrereqs();
		boolean prereqsMet = false;
		if (session == null) {
			statusEntity.setPrereqsInfo("Experiment not found");
		} else if (settings == null) {
			statusEntity.setPrereqsInfo("Could not access project settings");
		} else { 
			if (ResourceUtils.hasResource(session,settings.getStructuralPreprocResource())) {
				prereqsMet = true;
			} else {
				statusEntity.setPrereqsInfo("Experiment does not have a " + settings.getStructuralPreprocResource() + " resource.");
			}
		}
		try {
			if (!prereqs && prereqsMet) {
				statusEntity.setPrereqs(true);
				statusEntity.setPrereqsTime(new Date());
				statusEntity.setPrereqsInfo("");
				
			} else if (prereqs && !prereqsMet) {
				statusEntity.setPrereqs(false);
				statusEntity.setPrereqsTime(new Date());
			}
		} catch (Exception e) {
			log.debug(e.toString());
		}
	}

}
