package org.nrg.structuralqc.components.pcp;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.common.utilities.components.NodeUtils;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.pcp.anno.PipelineSubmitter;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.structuralqc.components.ProjectSettings;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.structuralqc.utils.StructuralQcUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;

import com.google.common.collect.ImmutableList;

@PipelineSubmitter
public class StructuralQcSubmitter implements PipelineSubmitterI {
	
	private ProjectSettings _projectSettings = XDAT.getContextService().getBean(ProjectSettings.class);;
	private CatalogService _catalogService = XDAT.getContextService().getBean(CatalogService.class);;
	private final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
	
	@Override
	public List<String> getParametersYaml(final String projectId, final String pipelineId) {
		return ImmutableList.of();
	}

	@Override
	public boolean submitJob(final PcpStatusEntity entity, final PcpStatusEntityService entityService, final PipelineValidatorI validator,
			final Map<String, String> parameters, final UserI user) throws PcpSubmitException {
		
		final XnatMrsessiondata mrSession = XnatMrsessiondata.getXnatMrsessiondatasById(entity.getEntityId(), user, false);
		final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(entity.getProject());
		entity.setStatus(PcpConstants.PcpStatus.RUNNING.toString());
		final Date statusTime = new Date();
		entity.setStatusInfo("Status set to running at (TIME=" + statusTime + ", NODE=" + _nodeUtils.getXnatNode() + ")");
		entity.setStatusTime(statusTime);
		entity.setStatusTime(new Date());
		if (mrSession==null) {
			final String errMsg = "ERROR:  MR Session not found for entity: " + entity.toString();
			entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			throw new PcpSubmitException(errMsg);
		}
		entityService.update(entity);
		try {
			final ScriptResult scriptResult = StructuralQcUtils.runStructuralQcProcessing(mrSession, settings);
			if (scriptResult.isSuccess()) {
				if (_catalogService != null) {
					final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(mrSession.getProject())
							.append("/subjects/").append(mrSession.getSubjectId()).append("/experiments/").append(mrSession.getId())
							.append("/resources/").append(settings.getStructuralPreprocResource());
					_catalogService.refreshResourceCatalog(user, resourceSB.toString(), Operation.ALL);
					
				}
				entity.setStatus(PcpConstants.PcpStatus.COMPLETE.toString());
				entity.setStatusTime(new Date());
			} else {
				entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
				entity.setStatusTime(new Date());
				entity.setStatusInfo(scriptResult.getResult());
			}
		} catch (Exception e) {
			final String errMsg = "EXCEPTION:  Structural QC could not be run for " + entity.toString() + " - " + e.toString();
			entity.setStatus(PcpConstants.PcpStatus.ERROR.toString());
			entity.setStatusInfo(errMsg);
			entity.setStatusTime(new Date());
			throw new PcpSubmitException(errMsg, e);
		}
		entityService.update(entity);
		return true;
		
	}

}
