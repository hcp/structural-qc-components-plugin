package org.nrg.structuralqc.components.pcp;

import java.util.Date;

import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.structuralqc.components.ProjectSettings;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.structuralqc.utils.StructuralQcUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@PipelineValidator
public class StructuralQcValidator implements PipelineValidatorI {
		
	private ProjectSettings _projectSettings = XDAT.getContextService().getBean(ProjectSettings.class);;
	
	@Override
	public void validate(PcpStatusEntity statusEntity, UserI user) {
		final XnatMrsessiondata session = XnatMrsessiondata.getXnatMrsessiondatasById(statusEntity.getEntityId(), user, false);
		final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(statusEntity.getProject());
		final Boolean validated = statusEntity.getValidated();
		boolean isValid = false;
		if (session == null) {
			statusEntity.setValidatedInfo("Experiment not found");
		} else if (settings == null) {
			statusEntity.setValidatedInfo("Could not access project settings");
		} else { 
			if (ResourceUtils.hasResource(session,settings.getStructuralPreprocResource())) {
				final XnatResourcecatalog resource = ResourceUtils.getResource(session,settings.getStructuralPreprocResource());
				if (StructuralQcUtils.hasStructuralQcOutput(resource)) {
					isValid = true;
				} else {
					statusEntity.setValidatedInfo("Resource " + settings.getStructuralPreprocResource() + " does not have StructuralQC output.");
				}
			} else {
				statusEntity.setPrereqsInfo("Experiment does not have a " + settings.getStructuralPreprocResource() + " resource.");
			}
		}
		try {
			if (!validated && isValid) {
				statusEntity.setValidated(true);
				statusEntity.setValidatedTime(new Date());
				statusEntity.setValidatedInfo("");
				
			} else if (validated && !isValid) {
				statusEntity.setValidated(false);
				statusEntity.setValidatedTime(new Date());
			}
		} catch (Exception e) {
			log.debug(e.toString());
		}
	}

}
