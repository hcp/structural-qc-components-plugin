package org.nrg.structuralqc.components.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "structuralQcComponentsPlugin",
			name = "Structural QC Components Plugin"
			//,
			//log4jPropertiesFile = "/META-INF/resources/structuralQcLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.structural.qc.components.conf"
	})
@Slf4j
public class CcfStructuralQcComponentsPlugin {
	
	public CcfStructuralQcComponentsPlugin() {
		log.info("Configuring the Structural QC Components Plugin.");
	}
	
}
